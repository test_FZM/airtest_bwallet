# -*- encoding=utf8 -*-
__author__ = "xiaomi"

from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
import sys
sys.path.append(".")
import addFrends

poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

#amount（金额），suc（1正例），msg（用例名称）
def pay_DCRWithFrends(amount,suc,msg):
    stop_app('com.fzm.wallet')
    start_app('com.fzm.wallet')
    sleep(5)
#     touch(Template(r"tpl1550647453420.png", record_pos=(0.336, 0.947), resolution=(1080, 2160)))
#     touch(Template(r"tpl1550647476741.png", record_pos=(-0.344, -0.374), resolution=(1080, 2160)))
    poco("com.fzm.wallet:id/ly_my").click()#点击‘我的’
    sleep(5)
    poco("com.fzm.wallet:id/tv_contacts").click()#点击‘联系人’
    sleep(5)
    touch(Template(r"tpl1550737747171.png", record_pos=(-0.328, -0.165), resolution=(1080, 2160)))
#     touch(Template(r"tpl1550737762135.png", record_pos=(-0.343, -0.28), resolution=(1080, 2160)))
    poco("com.fzm.wallet:id/root").click()#点击联系人中的地址
#     touch(Template(r"tpl1550645220292.png", record_pos=(0.005, 0.86), resolution=(1080, 2160)))
    poco("com.fzm.wallet:id/btn_ok").click()#点击‘去转账’
    sleep(5)
#     touch(Template(r"tpl1550646641079.png", record_pos=(-0.279, -0.215), resolution=(1080, 2160)))
    poco("com.fzm.wallet:id/et_money").click()#点击金额输入框
    text(amount)
    touch(Template(r"tpl1550635297148.png", record_pos=(-0.379, 0.083), resolution=(1080, 2160)))
#     touch(Template(r"tpl1550646884789.png", record_pos=(-0.004, 0.697), resolution=(1080, 2160)))
    poco("com.fzm.wallet:id/btn_out").click()#点击‘确认转币’
    sleep(5)
    if(suc==1):
        try:
            assert_exists(Template(r"tpl1550643344693.png", record_pos=(0.006, 0.12), resolution=(1080, 2160)), msg)
        except:
            print(msg+'失败')
    else:
        try:
            assert_not_exists(Template(r"tpl1550643344693.png", record_pos=(0.006, 0.12), resolution=(1080, 2160)), msg)
        except:
            print(msg+'失败')

addFrends.addFrends('DCR',addFrends.phone_number,2,addFrends.DCR_addr)
# pay_DCRWithFrends('0.0001',1,'正常转币')
# pay_DCRWithFrends('',0,'空金额转币')
pay_DCRWithFrends('1000000',0,'超额支付')
# pay_DCRWithFrends('非数字',0,'非数字')
