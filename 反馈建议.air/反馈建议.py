# -*- encoding=utf8 -*-
__author__ = "xiaomi"

from airtest.core.api import *
import os
from poco.drivers.android.uiautomation import AndroidUiautomationPoco


poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

#suggest建议，phone联系方式，pic（0无图，1一图，2二图），suc（1正例，2反例），mas返回信息
def response(suggest,phone,pic,suc,msg):
    clear_app('com.fzm.wallet')
    start_app('com.fzm.wallet')
    #手机型号确认,区分通知ui
    msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|awk 'NR==2{print $4}'|grep MIX_2S")
    if msg == 0:
        poco("android:id/button1").click()
    else:
        poco("com.android.packageinstaller:id/permission_allow_button").click()
    
    poco("com.fzm.wallet:id/btn_create_fast").click()#创建小钱包
    sleep(15)
    poco("com.fzm.wallet:id/ly_my").click()#点击我的
    poco("com.fzm.wallet:id/tv_about").click()#点击关于我们
    poco("com.fzm.wallet:id/tv_use_advice").click()#点击反馈建议
    sleep(5)
    poco("android.widget.LinearLayout").offspring("com.fzm.wallet:id/stateView").child("android.webkit.WebView").offspring("app").child("android.view.View").child("android.view.View")[0].offspring("textarea").click()
    #点击建议输入框
    text(suggest)
    poco("android.widget.EditText").click()#点击联系方式
    text(phone)
    if(pic == 1):
        touch(Template(r"tpl1550567223765.png", record_pos=(-0.194, -0.017), resolution=(1080, 2220)))
        poco("com.android.documentsui:id/drawer_layout").child("android.widget.LinearLayout").child("android.widget.FrameLayout").offspring("com.android.documentsui:id/container_directory").offspring("com.android.documentsui:id/dir_list").offspring("android.view.View").click()

    elif(pic ==2):
        touch(Template(r"tpl1550567223765.png", record_pos=(-0.194, -0.017), resolution=(1080, 2220)))
        poco("com.android.documentsui:id/drawer_layout").child("android.widget.LinearLayout").child("android.widget.FrameLayout").offspring("com.android.documentsui:id/container_directory").offspring("com.android.documentsui:id/dir_list").offspring("android.view.View").click()

        touch(Template(r"tpl1550567223765.png", record_pos=(-0.194, -0.017), resolution=(1080, 2220)))
        poco("com.android.documentsui:id/drawer_layout").child("android.widget.LinearLayout").child("android.widget.FrameLayout").offspring("com.android.documentsui:id/container_directory").offspring("com.android.documentsui:id/dir_list").offspring("android.view.View").click()
    touch(Template(r"tpl1550566607145.png", record_pos=(0.009, 0.769), resolution=(1080, 2220)))
    if(suc == 1):
        try:
            assert_exists(Template(r"tpl1550569022673.png", record_pos=(-0.001, -0.058), resolution=(1080, 2220)), msg)
        except:
            print(msg+'失败')
    else:
        try:
            assert_not_exists(Template(r"tpl1550569022673.png", record_pos=(-0.001, -0.058), resolution=(1080, 2220)), msg)
        except:
            print(msg+'失败')
    
#不提交图片
response(suggest='test',phone='tester',pic=0,suc=1,msg='无图提交建议')
#无建议
# response(suggest='',phone='tester',pic=0,suc=0,msg='无建议')
#无联系方式
# response(suggest='test',phone='',pic=0,suc=1,msg='无联系方式')
#1图
# response(suggest='test',phone='tester',pic=1,suc=1,msg='1图提交建议')
#多图
# response(suggest='test',phone='tester',pic=2,suc=1,msg='2图提交建议')

