# -*- encoding=utf8 -*-

import configparser, os, sys
import re


#用例存放总路径

parentpath =  os.getcwd()

#遍历总路径获取路径下全部用例
def airpath(parentpath):
    list = os.listdir(parentpath)
    paths = []
    for i in range(0,len(list)):
        childpath = parentpath + '/' + list[i]
        if os.path.isdir(childpath):
            if re.search('\.air', childpath):
                paths.append(childpath)
            else:
                otherpath = airpath(childpath)
                if otherpath != []:
                    paths.extend(otherpath)
    return paths

#生成执行用例命令
def command_run(case_path,case_name):
    return 'airtest run' + " " + case_path + " " + '--log '+ case_name + "/log >" + case_name + ".log 2>&1"

#生成用例报告命令
def command_report(case_name):
    return 'airtest report' + " " + case_name + " " + '--export ../report'

#根据日志获取用例结果
def result_log(case_path,case_name):
    #log_path = case_path + "/../../report/" + case_name.split(".")[0] + ".log/"
    #log_file = "log.html"
    log_path = case_path + "/../"
    log_file = case_name + ".log"
    print(log_path+log_file)
    result = "sucess"
    if os.path.isfile(log_path+log_file):
        with open(log_path+log_file,"r")as logs:
            lines = logs.readlines()
            for line in lines:
                if re.match("FAILED",line):
                    result = "failed"
    else:
        result = "no log exist"
    return result

#输出用例执行结果
def result_print(fail_case_count,case_count,fail_case_result):
    print("用例执行结果如下：")
    print("all case count:",case_count)
    print("fail case count:",fail_case_count)
    print("失败用例如下：")
    for key in fail_case_result.keys():
        print(key + ":" + fail_case_result[key])


#总的执行流程
alltestpath = airpath(parentpath)
fail_case_result = {}
case_result = {}
case_count = 0
fail_case_count = 0
for i in range(0,len(alltestpath)):
    case_path = alltestpath[i]
    case_name = os.path.basename(alltestpath[i])
    cmd = command_run(case_path,case_name)
    cmd_report = command_report(case_name)
    os.system(cmd)
    os.system(cmd_report)
    result = result_log(case_path,case_name)
    if result == "failed":
        fail_case_count += 1
        fail_case_result[case_name] = result
    case_count += 1
    case_result[case_name] = result
#失败用例打印
result_print(fail_case_count,case_count,fail_case_result)




