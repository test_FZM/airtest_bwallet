# -*- encoding=utf8 -*-
__author__ = "xiaomi"

from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
import os


poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

#name(联系人姓名),phone(联系方式),i币种(0BTC,1BTY,2DCR,3ETH,4YCC)addr(转币地址),suc(1正例，0反例），msg(用例内容)
def addFrends(name,phone,i,addr,suc,msg):
    clear_app('com.fzm.wallet')
    start_app('com.fzm.wallet')
    #手机型号确认,区分通知ui
    msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|awk 'NR==2{print $4}'|grep MIX_2S")
    if msg == 0:
        poco("android:id/button1").click()
    else:
        poco("com.android.packageinstaller:id/permission_allow_button").click()
    poco("com.fzm.wallet:id/btn_create_fast").click()
    sleep(15)
    poco("com.fzm.wallet:id/ly_my").click()#点击‘我的’
    poco("com.fzm.wallet:id/tv_contacts").click()#点击‘联系人’
    poco("添加联系人").click()
    poco("com.fzm.wallet:id/et_nick_name").click()#姓名输入框
    text(name)
    poco("com.fzm.wallet:id/et_phone").click()#号码输入框
    text(phone)
    poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
    poco("android.widget.LinearLayout").offspring("com.fzm.wallet:id/drawer_layout").offspring("com.fzm.wallet:id/rv_list").child("android.widget.LinearLayout")[i].click()
    #选择币种
    poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
    text(addr)
    poco("android.widget.TextView").click()#点击‘保存’
    if(suc==1):
        try:
            assert_exists(Template(r"tpl1550473030845.png", record_pos=(-0.009, -0.891), resolution=(1080, 2220)), msg)
        except:
            print(msg+'失败')
    else:
        try:
            assert_not_exists(Template(r"tpl1550473030845.png", record_pos=(-0.009, -0.891), resolution=(1080, 2220)), msg)
        except:
            print(msg+'失败')

            
BTC_addr='1Cq4xbNaoj4r7uvFWx76ChnfwUKCrqeor5'
BTY_addr='1PZ9VgBLiE97GQqfAjVPAnvhU1r3ADgETw'
DCR_addr='Dsk7SF346GKoBtTx7BJjimHQkd7uCeLXudc'
ETH_addr='0xa9570e5bd66eA3747dF0ab735c87DAE2c312ac9E'
YCC_addr='0xa9570e5bd66eA3747dF0ab735c87DAE2c312ac9E'
phone_number='13900000000'

#right_with_BTC
# addFrends('BTC',phone_number,0,BTC_addr,1,'添加BTC地址')
#none_name
# addFrends('',phone_number,0,BTC_addr,0,'不输入姓名')
#none_address
# addFrends('BTC',phone_number,0,'',0,'不输入地址')
#right_with_BTY
addFrends('BTY',phone_number,1,BTY_addr,1,'添加BTY地址')
#right_with_DCR
# addFrends('DCR',phone_number,2,DCR_addr,1,'添加DCR地址')
#right_with_ETH
# addFrends('ETH',phone_number,3,ETH_addr,1,'添加ETH地址')
#right_with_YCC
addFrends('YCC',phone_number,4,YCC_addr,1,'添加YCC地址')


#添加多条地址
# stop_app('com.fzm.wallet')
# start_app('com.fzm.wallet')
# poco("com.fzm.wallet:id/ly_my").click()#点击‘我的’
# poco("com.fzm.wallet:id/tv_contacts").click()#点击‘联系人’
# poco("添加联系人").click()
# poco("com.fzm.wallet:id/et_nick_name").click()#姓名输入框
# text('BTC')
# poco("com.fzm.wallet:id/et_phone").click()#号码输入框
# text(phone_number)
# poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
# poco("android.widget.LinearLayout")[0].click()#选择币种
# poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
# text(BTC_addr)
# poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
# poco("android.widget.LinearLayout")[1].click()#选择币种
# poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
# text(BTY_addr)
# poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
# poco("android.widget.LinearLayout")[2].click()#选择币种
# poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
# text(DCR_addr)
# poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
# poco("android.widget.LinearLayout")[3].click()#选择币种
# poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
# text(ETH_addr)
# poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
# poco("android.widget.LinearLayout")[4].click()#选择币种
# poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
# text(YCC_addr)