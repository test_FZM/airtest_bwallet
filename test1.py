# coding=utf-8
import turtle
import time

# 同时设置pencolor=color1, fillcolor=color2
turtle.color("red", "yellow")

turtle.begin_fill()
for i in range(5):
	turtle.hideturtle()
	turtle.forward(200)
	turtle.left(144)

turtle.end_fill()

turtle.mainloop()
