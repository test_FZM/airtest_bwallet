# -*- encoding=utf8 -*-
__author__ = "xiaomi"

from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
import os


poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)
#环境准备
clear_app('com.fzm.wallet')
start_app('com.fzm.wallet')
#手机型号确认,区分通知ui
msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|grep MIX_2S")
if msg == 0:
    poco("android:id/button1").click()
else:
    poco("com.android.packageinstaller:id/permission_allow_button").click()


#创建小钱包
poco("com.fzm.wallet:id/btn_create_fast").click()

sleep(5)
#钱包管理界面
poco("com.fzm.wallet:id/ly_my").click()
poco("com.fzm.wallet:id/tv_manage_wallet").click()
#删除钱包
msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|grep MIX_2S")
if msg == 0:
    swipe(Template(r"tpl1551756818267.png", record_pos=(0.003, -0.414), resolution=(1080, 2160)), vector=[-0.4839, -0.0047])#小米手机
else:
    poco("com.fzm.wallet:id/tv_current_wallet").swipe([-0.5646, 0.0037])
    poco("com.fzm.wallet:id/tv_current_wallet").swipe([-0.7936, -0.0073])

    #poco("com.fzm.wallet:id/tv_amount").swipe([-0.4068, 0.0])

    #poco("com.fzm.wallet:id/tv_current_wallet").swipe([-0.3114, -0.0037])




