# -*- encoding=utf8 -*-
__author__ = "liug"

import configparser, os
from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco

poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)
auto_setup(__file__)

clear_app('com.fzm.wallet')
start_app('com.fzm.wallet')
# 手机型号确认,区分通知ui
msg = os.system(
    "/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|awk 'NR==2{print $4}'|grep MIX_2S")
if msg == 0:
    poco("android:id/button1").click()
else:
    poco("com.android.packageinstaller:id/permission_allow_button").click()
#界面统一从主页进入
poco("com.fzm.wallet:id/btn_import").child("android.widget.ImageView").click()

#执行路径
path =  os.getcwd()

#配置
config = configparser.ConfigParser()
config.read(path+"/config.cfg",encoding='utf8')

wallet_word = config.get("importwallet1","wallet_word").split()
password = config.get("importwallet1","password")
wallet_maxnu = config.get("importwallet1","wallet_number")
#导入钱包
class import_wallet:
    def walletin(self,name = "test_account",wallet_words = [],pawd = "null"):        
        
        for i in range(1,6):           
            poco("com.fzm.wallet:id/et_block"+str(i)).click()
            text(wallet_words[i-1])
        poco("com.fzm.wallet:id/et_name").click()
        text(name)
        poco("com.fzm.wallet:id/et_password").click()
        text(pawd)
        poco("com.fzm.wallet:id/et_password_again").click()
        text(pawd)
        poco("com.fzm.wallet:id/btn_import").click()
        wait(Template(r"tpl1545904233543.png", record_pos=(0.001, 0.103), resolution=(1080, 2160)))

        return "wallet import success"

inwallet = import_wallet()
test_result = inwallet.walletin(wallet_words = wallet_word, pawd = password)
print (test_result)


snapshot(msg="请填写测试点.")
sleep(1.0)
exists(Template(r"tpl1550195303364.png", record_pos=(-0.02, 0.086), resolution=(1080, 2160)))





