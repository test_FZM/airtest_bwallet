# -*- encoding=utf8 -*-
__author__ = "xiaomi"

from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
import os


poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

BTC_addr='1Cq4xbNaoj4r7uvFWx76ChnfwUKCrqeor5'
BTY_addr='1PZ9VgBLiE97GQqfAjVPAnvhU1r3ADgETw'
DCR_addr='Dsk7SF346GKoBtTx7BJjimHQkd7uCeLXudc'
ETH_addr='0xa9570e5bd66eA3747dF0ab735c87DAE2c312ac9E'
YCC_addr='0xa9570e5bd66eA3747dF0ab735c87DAE2c312ac9E'
phone_number='13900000000'

#name(联系人姓名),phone(联系方式),i币种(0BTC,1BTY,2DCR,3ETH,4YCC),addr(转币地址),suc(1正例，0反例），msg(用例内容)
def addFrends(name,phone,i,addr):
    clear_app('com.fzm.wallet')
    start_app('com.fzm.wallet')
    sleep(5)
    #手机型号确认,区分通知ui
    msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|awk 'NR==2{print $4}'|grep MIX_2S")
    if msg == 0:
        poco("android:id/button1").click()
    else:
        poco("com.android.packageinstaller:id/permission_allow_button").click()

    poco("com.fzm.wallet:id/btn_import").click()
    poco("com.fzm.wallet:id/et_block1").click()
    text('物沿乘')
    poco("com.fzm.wallet:id/et_block2").click()
    text('跑苏曹')
    poco("com.fzm.wallet:id/et_block3").click()
    text('情极胀')
    poco("com.fzm.wallet:id/et_block4").click()
    text('纲钢医')
    poco("com.fzm.wallet:id/et_block5").click()
    text('细税招')

    poco("com.fzm.wallet:id/et_name").click()
    text('钱包仇')
    poco("com.fzm.wallet:id/et_password").click()
    text('abcd1234')
    poco("com.fzm.wallet:id/et_password_again").click()
    text('abcd1234')
    poco("com.fzm.wallet:id/btn_import").click()
    poco("com.fzm.wallet:id/ly_my").click()#点击‘我的’
    sleep(3)
    poco("com.fzm.wallet:id/tv_contacts").click()#点击‘联系人’
    sleep(3)
    poco("添加联系人").click()
    sleep(3)
    poco("com.fzm.wallet:id/et_nick_name").click()#姓名输入框
    text(name)
    poco("com.fzm.wallet:id/et_phone").click()#号码输入框
    text(phone)
    poco("com.fzm.wallet:id/tv_coin_type").click()#添加币种
    sleep(3)
    poco("android.widget.LinearLayout").offspring("com.fzm.wallet:id/drawer_layout").offspring("com.fzm.wallet:id/rv_list").child("android.widget.LinearLayout")[i].click()#选择币种
    sleep(2)
    poco("com.fzm.wallet:id/et_address").click()#转币地址输入框
    text(addr)
    poco("android.widget.TextView").click()#点击‘保存’
