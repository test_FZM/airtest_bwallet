# -*- encoding=utf8 -*-
__author__ = "liug"

from airtest.core.api import *
import configparser
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

#执行路径
path =  os.getcwd()

#配置
config = configparser.ConfigParser()
config.read(path+"/config.cfg",encoding='utf8')

wallet_word = config.get("importwallet2","wallet_word").split()
password = config.get("importwallet2","password")
address = config.get("importwallet2","bty_address")

class import_wallet:
    def walletin(self,name = "null",wallet_words = [],pawd = "null"):        
        poco("com.fzm.wallet:id/btn_import").click()
        
        for i in range(1,6):           
            poco("com.fzm.wallet:id/et_block"+str(i)).click()
            text(wallet_words[i-1])
        poco("com.fzm.wallet:id/et_name").click()
        text(name)
        poco("com.fzm.wallet:id/et_password").click()
        text(pawd)
        poco("com.fzm.wallet:id/et_password_again").click()
        text(pawd)
        poco("com.fzm.wallet:id/btn_import").click()
        wait(Template(r"tpl1545904233543.png", record_pos=(0.001, 0.103), resolution=(1080, 2160)))

        return "wallet import success"
#环境清理
clear_app("com.fzm.wallet")
#开始新的环境
start_app("com.fzm.wallet")
#手机型号确认,区分通知ui
msg = os.system("/usr/local/lib/python3.5/site-packages/airtest/core/android/static/adb/linux/adb devices -l|awk 'NR==2{print $4}'|grep MIX_2S")
if msg == 0:
    poco("android:id/button1").click()
else:
    poco("com.android.packageinstaller:id/permission_allow_button").click()
inwallet = import_wallet()
inwallet_result = inwallet.walletin(name = "transfer_acc",wallet_words = wallet_word,pawd = password)

#bty转账
poco("com.fzm.wallet:id/rv_list").child("android.widget.LinearLayout")[0].child("android.widget.LinearLayout").child("com.fzm.wallet:id/iv_coin").click()
poco(text="转账").click()
poco("com.fzm.wallet:id/et_money").click()
text("100")
poco("com.fzm.wallet:id/et_note").click()
text("bty autotest transfer")
poco("com.fzm.wallet:id/tv_other_address").click()
#钱包地址
text(address)
poco("com.fzm.wallet:id/btn_out").click()
#poco("com.fzm.wallet:id/et_input").click()
#text(password)
#poco("com.fzm.wallet:id/btn_right").click()
assert_exists(Template(r"tpl1551927214268.png", record_pos=(0.013, 0.75), resolution=(1080, 2160)), "余额不足")


